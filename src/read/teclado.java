/*
 * Libreria de funciones para leer datos de teclado
 */
package read;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alu2016340
 */
public class teclado {

    public static String pedirCadena(String texto) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String cadena = "";
        do {
            try {
                System.out.print(texto);
                cadena = br.readLine();
                if (cadena.equals("")) {
                    System.out.println("No se puede dejar el campo en blanco.");
                }
            } catch (IOException ex) {
                System.out.println("Error de entrada / salida.");
            }
        } while (cadena.equals(""));
        return cadena;
    }

    public static char pedirChar(String texto) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String cadena;
        char ch = ' ';
        do {
            try {
                System.out.print(texto);
                cadena = br.readLine();
                if (cadena.equals("")) {
                    System.out.println("No se puede dejar el campo en blanco.");
                }
                if (cadena.length() > 1) {
                    System.out.println("Introduce solo un caracter");
                } else {
                    ch = cadena.charAt(0);
                }
            } catch (IOException ex) {
                System.out.println("Error de entrada / salida.");
            }
        } while (ch == ' ');
        return ch;
    }

    public static char pedirChar(String texto, List<Character> list) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String cadena;
        char ch = ' ';
        do {
            try {
                System.out.print(texto);
                cadena = br.readLine();
                if (cadena.equals("")) {
                    System.out.println("No se puede dejar el campo en blanco.");
                }
                if (cadena.length() > 1) {
                    System.out.println("Introduce solo un caracter");
                } else if (!list.contains(cadena.charAt(0))) {
                    System.out.println("El character tiene que ser uno de los siguientes: ");
                    list.stream().forEach((c) -> {
                        System.out.print(c + " ");
                    });
                    System.out.println("");
                } else {
                    ch = cadena.charAt(0);
                }
            } catch (IOException ex) {
                System.out.println("Error de entrada / salida.");
            }
        } while (ch == ' ');
        return ch;
    }

    public static double pedirDouble(String texto) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        double num = 0;
        boolean error;
        do {
            try {
                System.out.print(texto);
                num = Double.parseDouble(br.readLine());
                error = false;
            } catch (IOException ex) {
                System.out.println("Error de entrada / salida.");
                error = true;
            } catch (NumberFormatException ex) {
                System.out.println("Debes introducir un número.");
                error = true;
            }
        } while (error);
        return num;
    }

    public static int pedirEntero(String texto) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = 0;
        boolean error;
        do {
            try {
                System.out.print(texto);
                num = Integer.parseInt(br.readLine());
                error = false;
            } catch (IOException ex) {
                System.out.println("Error de entrada / salida.");
                error = true;
            } catch (NumberFormatException ex) {
                System.out.println("Debes introducir un número entero.");
                error = true;
            }
        } while (error);
        return num;
    }
    
     public static int pedirEntero(String texto, int min, int max) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int num = 0;
        boolean error;
        do {
            try {
                System.out.print(texto);
                num = Integer.parseInt(br.readLine());
                if(num>=min && num<=max){
                    
                    error = false;
                }else{
                    System.out.println("El numero deve estar entre " + min + " y "  + max);
                    error = true;
                }
                
            } catch (IOException ex) {
                System.out.println("Error de entrada / salida.");
                error = true;
            } catch (NumberFormatException ex) {
                System.out.println("Debes introducir un número entero.");
                error = true;
            }
        } while (error);
        return num;
    }

    // Método que indica si un String es un entero o no
    public static boolean esEntero(String numero) {
        try {
            Integer.parseInt(numero);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }

}
